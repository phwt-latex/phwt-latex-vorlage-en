# TODOs:

- English oder Deutsch? Entscheiden
- packages.tex is still chaos
- Acronyms / Glossary style, decide:
  1. Acronyms show up in Glossary and Acronym list
  2. Acronyms that are in Glossary do not sho up
- Anleitung um Build umgebung richtig aufzusetzen


## Vor der Abgabe:

- Suche nach ?? und TODO in pdf
- Erklaerung.tex checken ob noch passt
- letztes Mal pdf auf rechtschriebung (zb word copy paste)
- druber fliegen, ob irgendwo formattierung verkackt
  - Insbesondere Bilder! die sonstwo rumfliegen
- code beispiele lesen
- schauen, dass rand nicht ueberschritten (overfull hbox warning mit mehr als 1-3pt)
- Abkuerzungen einheitlich in \gls oder zumindest immer konsistent (z.B. immer ADC statt adc, Adc, )
- kB / MB evtl durch {\kilo\byte} ersetzen

