# LaTeX Vorlage PHWT 

- Muss gepflegt werden. Packete veralten, neue kommen dazu.
- LaTeX compiler Fehler sind meist nutzlos -> Copy paste in google :).
- `tex.stackexchange.com` ist das Stackoverflow fuer LaTeX. Fast alle Dinge sind dort beantwortet
- Oft hilft es bei Problemen ein MWE (Minimal Working Example) zu erstellen.


## Nutzung

### Schreiben

- VSCode installieren (Extensions: Latex Workshop, LTeX Spellchecker) 
- LaTeX Umgebung installieren (Tip: TexLive-full on Linux, MikTex on Windows)
- Dateien in vscode oeffnen ueber `CTRL + P`
- Metadaten (Name, Thema etc.) anpassen in `_meta.tex`.
- Abkürzungen in `acronyms.tex` adden.
- Beispiel Nutzung fuer Text in `chapters/10_intro.tex`.

### Literatur

- In `bibliography.bib` sind Literatureintraege. Kann man per Hand erstellen. 
- Besser:
- [Zotero](zotero.org) installieren, Zotero-Connector Browser add-on installieren.
- Im Browser erscheint dann ein Symbol bei den Add-ons. Drauf klicken -> Webpage ist in Zotero gespeichert.
- Fuer Buecher: Google scholar, Springer Link.
- In Zotero: `File > Export Library > Format: BibLaTex`
  - Bib**La**Tex ist richtig. Bib**T**ex ist falsch.
  - `bibliography.bib` ueberschreiben.
- Fertig, Literaturverweise im Text siehe `chapters/10_intro.tex`.


## Tricks

- Man kann Tabellen per Hand in LaTeX erstellen.
- Das Leben ist einfacher, wenn man sie in Excel erstellt, als csv exportiert und dann mit Python als LaTeX Tabelle darstellt.
- Es gibt Makros. Eher sparsam nutzen, wenn Sachen nerven. Z.B.:
  - `\newcommand{\oft}{\gls{word-i-use-VERY-often} }` (Problem: Leerzeichen dahinter notwendig...)
  - Auch gut als Platzhalter, wenn Begriff noch keinen festen Namen hat.
- Google Scholar für Listertur Quellen ist 1A.
- Grundlagen über LaTeX lesen.
- An der PHWT nutzen wir Git zur [Versionskontrolle](https://stackoverflow.com/a/1408464). You should, too.
- Die `.gitignore` datei nutzen, um bestimmte Dateien nicht in Git zu haben (große oder oft geänderte Dateien. Z.B. den output von LaTeX)
- Editoren wie "Texworks", "TexMaker" verblassen gg. vscode
- Erst schreiben, dann am Ende genug Zeit, um einmal Formatierung zu machen
  - Wenig whitespace. Wenn bilder etwas weiter weg sind ist okay.
  - Siehe: TODO link fuer guten latex stil.
- Powerful vscode features: Autocomplete, Toggle line comment with Shortcut (e.g. CTRL+/), Ctrl+Shift+F to Search/Replace in Workspace, Ctrl+P to open file, Ctrl+Shift+P to execute setting changes (just search for what you want to do).

## Packete installieren

- Ist nervig in Latex
- So wenig wie moeglich
- **Die *Reihenfolge* der Inkludierung ist wichtig**
  - Ja. Es das sit traurig, ich weiß.
- Siehe `packages.tex`


## Beispiel vscode config

Der `.vscode` Ordner enthaelt eine sinnvolle config. Falls er nicht existiert, hier eine gute `settings.json` fuer VSCode:

```json
{
    "editor.wordWrap": "on",
    "latex-workshop.view.pdf.viewer": "tab",
    "git.enableSmartCommit": true,
    // "latex-workshop.latex.autoBuild.run": "onSave", // Optional: Esp. lower powered PCs.
    // "latex-workshop.latex.autoBuild.interval": 360000, // Only auto build every 6 minutes
    // "editor.suggestSelection": "recentlyUsed", // Optional: Try if VSCode suggestions are crap
    // "[latex]": {
    //     "editor.suggestSelection": "recentlyUsed"
    // },
    "latex-workshop.latex.tools": [
        {
            "name": "latexmk",
            "command": "latexmk",
            "args": [
                "-shell-escape", // Needed for minted
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-pdf",
                "-outdir=%OUTDIR%",
                "%DOC%"
            ],
            "env": {}
        },
        {
            "name": "makeglossaries",
            "command": "makeglossaries",
            "args": [
                "%DOCFILE%"
            ]
        },
        {
            "name": "biber",
            "command": "biber",
            "args": [
                "%DOC%.bcf"
            ],
            "env": {}
        },
        {
            "name": "lualatexmk",
            "command": "latexmk",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-lualatex",
                "-outdir=%OUTDIR%",
                "%DOC%"
            ],
            "env": {}
        },
        {
            "name": "xelatexmk",
            "command": "latexmk",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-xelatex",
                "-outdir=%OUTDIR%",
                "%DOC%"
            ],
            "env": {}
        },
        {
            "name": "latexmk_rconly",
            "command": "latexmk",
            "args": [
                "%DOC%"
            ],
            "env": {}
        },
        {
            "name": "pdflatex",
            "command": "pdflatex",
            "args": [
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "%DOC%"
            ],
            "env": {}
        },
        {
            "name": "bibtex",
            "command": "bibtex",
            "args": [
                "%DOCFILE%"
            ],
            "env": {}
        },
        {
            "name": "rnw2tex",
            "command": "Rscript",
            "args": [
                "-e",
                "knitr::opts_knit$set(concordance = TRUE); knitr::knit('%DOCFILE_EXT%')"
            ],
            "env": {}
        },
        {
            "name": "jnw2tex",
            "command": "julia",
            "args": [
                "-e",
                "using Weave; weave(\"%DOC_EXT%\", doctype=\"tex\")"
            ],
            "env": {}
        },
        {
            "name": "jnw2texminted",
            "command": "julia",
            "args": [
                "-e",
                "using Weave; weave(\"%DOC_EXT%\", doctype=\"texminted\")"
            ],
            "env": {}
        },
        {
            "name": "pnw2tex",
            "command": "pweave",
            "args": [
                "-f",
                "tex",
                "%DOC_EXT%"
            ],
            "env": {}
        },
        {
            "name": "pnw2texminted",
            "command": "pweave",
            "args": [
                "-f",
                "texminted",
                "%DOC_EXT%"
            ],
            "env": {}
        },
        {
            "name": "tectonic",
            "command": "tectonic",
            "args": [
                "--synctex",
                "--keep-logs",
                "%DOC%.tex"
            ],
            "env": {}
        }
    ],
}
```
